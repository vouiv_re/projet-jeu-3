"use strict";

import { Item } from "./item";

export class ChoiceItem extends Item{
  constructor(name, kind, color, isClue, arrayDescription) {
    super(name, kind, `hsl(${color}, 100%, 75%)`, isClue);
    this.listDescription = this.getDescription(arrayDescription, color);
  }

  getDescription(data, color) {
    let listDescription = [];
    for (let i = 0; i < data.length; i++) {
      let item = new Item(data[i].name, data[i].kind, `hsl(${color}, 100%, 75%)`);
      listDescription.push(item);
    }
    return listDescription;
  }

  displayDescription() {
    let descriptionSection = document.querySelector('#description');
    descriptionSection.innerHTML = '';
    for (let i = 0; i < this.listDescription.length; i++) {
      let item = this.listDescription[i];

      let description = document.createElement('div');
      let img = document.createElement('img');

      img.src = item.image;
      img.alt = item.name;
      description.style.backgroundColor = item.color;
      img.style.height = "3vw";

      description.setAttribute('class', `d-flex flex-wrap align-content-center justify-content-center col-2`);
      
      description.appendChild(img);
      descriptionSection.appendChild(description);
    }
  }
}