import { Item } from "./item";

"use strict";

export class SceneryItem extends Item{
  constructor(name, kind, isClue, color) {
    super(name, kind);
    this.isClue = isClue;
    this.color = color;
  }
}