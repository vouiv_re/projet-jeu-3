import { Item } from "./item";

"use strict";

export class DescriptionItem extends Item{
  constructor(name, kind, color){
    super(name, kind);
    this.color = color;
  }
}